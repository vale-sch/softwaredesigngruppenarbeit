"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.RegisteredUser = void 0;
class RegisteredUser {
    constructor(_username, _password) {
        this.username = _username;
        this.password = _password;
    }
    playQuiz(Quiz) {
        console.log("Methode // playQuiz");
    }
    createQuiz() {
        console.log("Methode // createQuiz");
    }
    watchStats() {
        console.log("Methode // watchStats");
    }
}
exports.RegisteredUser = RegisteredUser;
//# sourceMappingURL=RegisteredUser.js.map